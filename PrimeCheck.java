import java.util.Scanner; //Gibt die Moeglichkeit zur Eingabe von Werten

public class PrimeCheck {

	public static void main(String[] args) {
		
		try (Scanner input = new Scanner(System.in)) { //Umgebung zur Annahme von Eingaben
			
			System.out.println("Zahl eingeben (maximal |9223372036854775807|)"); //Bittet um die Eingabe der Zahl
			
			long zahl = input.nextLong(); //Nimmt die Eingabe der Zahl entgegen und schreibt sie in die Variable zahl
			
			System.out.print("Die Zahl " + zahl + " ist "); //Gibt den ersten Teil des Resultats aus
			
			for(long teiler = 2; teiler < (zahl/2); teiler++) { //Prueft, ob die eingegebene Zahl durch eine Zahl bis zur ihrer Haelfte teilbar ist
			
				if(zahl % teiler == 0) { //falls ja
				
					System.out.print("keine Primzahl, weil sie durch " + teiler + " teilbar ist."); //Negative Antwort
					
					System.exit(0); //Ende des Programms
				
				}
				
			}
			
			System.out.print("eine Primzahl."); //Positive Antwort
			
			System.exit(0);//Ende des Programms
			
		}
	}

}